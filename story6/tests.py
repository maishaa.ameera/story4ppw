from django.test import TestCase
from django.apps import apps
from .apps import Story6Config
from django.test import Client
from .models import Kegiatan, Orang
from .forms import FormKegiatan, FormOrang
from django.urls import resolve, reverse
from .views import homekegiatan, addkegiatan, listkegiatan, deleteUser, register, deleteActivity

# Create your tests here.
class ModelTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(namakegiatan="belajar")
        self.orang = Orang.objects.create(namaorang="caca")

    def test_instance_created(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(Orang.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.kegiatan), "belajar")
        self.assertEqual(str(self.orang), "caca")

class FormTest(TestCase):

    def test_form_is_valid(self):
        form_kegiatan = FormKegiatan(data={
            "namakegiatan": "belajar",
        })
        self.assertTrue(form_kegiatan.is_valid())
        form_orang = FormOrang(data={
            'namaorang': "caca"
        })
        self.assertTrue(form_orang.is_valid())

    def test_form_invalid(self):
        form_kegiatan = FormKegiatan(data={})
        self.assertFalse(form_kegiatan.is_valid())
        form_orang = FormOrang(data={})
        self.assertFalse(form_orang.is_valid())

class UrlsTest(TestCase):

    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(namakegiatan="belajar")
        self.orang = Orang.objects.create(
            namaorang="pipo", kegiatan=Kegiatan.objects.get(namakegiatan="belajar"))
        self.homekegiatan = reverse("story6:homekegiatan")
        self.addkegiatan = reverse("story6:addkegiatan")
        self.listkegiatan = reverse("story6:listkegiatan")
        self.register = reverse("story6:register", args=[self.kegiatan.pk])
        self.delete = reverse("story6:delete", args=[self.orang.pk])
        self.deleteActivity = reverse("story6:deleteActivity", args=[self.kegiatan.pk])

    def test_homekegiatan_use_right_function(self):
        found = resolve(self.homekegiatan)
        self.assertEqual(found.func, homekegiatan)

    def test_listkegiatan_use_right_function(self):
        found = resolve(self.listkegiatan)
        self.assertEqual(found.func, listkegiatan)

    def test_addkegiatan_use_right_function(self):
        found = resolve(self.addkegiatan)
        self.assertEqual(found.func, addkegiatan)

    def test_register_use_right_function(self):
        found = resolve(self.register)
        self.assertEqual(found.func, register)

    def test_delete_use_right_function(self):
        found = resolve(self.delete)
        self.assertEqual(found.func, deleteUser)
     
    def test_deleteActivity_use_right_function(self):
        found = resolve(self.deleteActivity)
        self.assertEqual(found.func, deleteActivity)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.homekegiatan = reverse("story6:homekegiatan")
        self.listkegiatan = reverse("story6:listkegiatan")
        self.addkegiatan = reverse("story6:addkegiatan")

    def test_GET_homekegiatan(self):
        response = self.client.get(self.homekegiatan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homekegiatan.html')

    def test_GET_listkegiatan(self):
        response = self.client.get(self.listkegiatan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'listkegiatan.html')

    def test_GET_addkegiatan(self):
        response = self.client.get(self.addkegiatan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addkegiatan.html')

    def test_POST_addkegiatan(self):
        response = self.client.post(self.addkegiatan,
                                    {
                                        'namakegiatan': 'belajar',
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addkegiatan_invalid(self):
        response = self.client.post(self.addkegiatan,
                                    {
                                        'namakegiatan': '',
                                    }, follow=True)
        self.assertTemplateUsed(response, 'addkegiatan.html')

    def test_GET_delete(self):
        kegiatan = Kegiatan(namakegiatan="abc")
        kegiatan.save()
        orang = Orang(namaorang="bobo",
                      kegiatan=Kegiatan.objects.get(pk=1))
        orang.save()
        response = self.client.get(reverse('story6:delete', args=[orang.pk]))
        self.assertEqual(Orang.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

    def test_GET_deleteActivity(self):
        kegiatan = Kegiatan(namakegiatan="xyz")

        kegiatan.save()
        orang = Orang(namaorang="bobi",
                      kegiatan=Kegiatan.objects.get(pk=1))
        orang.save()
        response = self.client.get(reverse('story6:deleteActivity', args=[kegiatan.pk]))
        self.assertEqual(Kegiatan.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(namakegiatan="abc")
        kegiatan.save()

    def test_regist_POST(self):
        response = Client().post('/story6/register/1/',
                                 data={'namaorang': 'zuzuzu'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/story6/register/1/')
        self.assertTemplateUsed(response, 'register.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post('/story6/register/1/',
                                 data={'nama': ''})
        self.assertTemplateUsed(response, 'register.html')
        # self.assertEqual(response.status_code, 302)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'story6')
        self.assertEqual(apps.get_app_config('story6').name, 'story6')









