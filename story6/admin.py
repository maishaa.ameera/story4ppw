from django.contrib import admin

from .models import Kegiatan
from .models import Orang

# Register your models here.
admin.site.register(Orang)
admin.site.register(Kegiatan)