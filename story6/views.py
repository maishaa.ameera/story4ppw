from django.shortcuts import render ,redirect
from .forms import FormKegiatan, FormOrang
from .models import Kegiatan, Orang
from datetime import datetime
from . import forms

# Create your views here.

def homekegiatan(request):
    return render(request, 'homekegiatan.html')

def addkegiatan(request):
    form_kegiatan = forms.FormKegiatan()
    if request.method == 'POST':
        form_kegiatan_input = forms.FormKegiatan(request.POST)
        if form_kegiatan_input.is_valid():
            data = form_kegiatan_input.cleaned_data
            kegiatan_input = Kegiatan()
            kegiatan_input.namakegiatan = data['namakegiatan']
            kegiatan_input.save()
            current_data = Kegiatan.objects.all()
            return redirect('story6:homekegiatan')  
        else:
            current_data = Kegiatan.objects.all()
            return render(request, 'addkegiatan.html',{'form':form_kegiatan, 'status':'failed','data':current_data})
    else:
        current_data = Kegiatan.objects.all()
        return render(request, 'addkegiatan.html',{'form':form_kegiatan,'data':current_data})

def listkegiatan(request):
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all()
    return render(request, 'listkegiatan.html', {'kegiatan': kegiatan, 'orang': orang})


def deleteUser(request, delete_id):
    # if request.method == "POST":
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all()
    print(delete_id)
    orang_to_delete = Orang.objects.get(id=delete_id)
    orang_to_delete.delete()
    # return render(request, 'story6/listActivity.html', {'kegiatan': kegiatan, 'orang': orang})
    return redirect('story6:listkegiatan')

def deleteActivity(request, delete_id):
    # if request.method == "POST":
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all()
    print(delete_id)
    kegiatan_to_delete = Kegiatan.objects.get(id=delete_id)
    kegiatan_to_delete.delete()
    # return render(request, 'story6/listActivity.html', {'kegiatan': kegiatan, 'orang': orang})
    return redirect('story6:listkegiatan')


def register(request, task_id):
    form_orang = FormOrang()
    if request.method == "POST":
        form_orang_input = FormOrang(request.POST)
        # print('oi')
        if form_orang_input.is_valid():
            data = form_orang_input.cleaned_data
            orangBaru = Orang()
            orangBaru.namaorang = data['namaorang']
            orangBaru.kegiatan = Kegiatan.objects.get(id=task_id)
            orangBaru.save()
            return redirect('story6:listkegiatan')
        else:
            return render(request, 'register.html', {'form': form_orang, 'status': 'failed'})
    else:
        return render(request, 'register.html', {'form': form_orang})

