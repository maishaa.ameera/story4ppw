from django import forms

class FormKegiatan(forms.Form):
    namakegiatan = forms.CharField(
        label = "Nama Kegiatan", 
        widget = forms.TextInput(
            attrs={
            'placeholder' : 'Enter nama kegiatan ',
            'type' : 'text',
            'id' : 'namakegiatan',
            'class':'form-control',
            'required': True,

            }
        )
    )

class FormOrang(forms.Form):
    namaorang = forms.CharField(
        label="Nama",
        max_length=64,
        widget=forms.TextInput(
            attrs={
                'placeholder' : 'Enter nama kamu ',
                'type' : 'text',
                'id' : 'nama',
                'class': 'form-control',
            }
        )
    )
    
