from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('homekegiatan/',views.homekegiatan, name='homekegiatan'),
    path('addkegiatan/',views.addkegiatan,name = 'addkegiatan'),
    path('listkegiatan/',views.listkegiatan, name='listkegiatan'),
    path('register/<int:task_id>/', views.register, name='register'),
    path('delete/<int:delete_id>/', views.deleteUser, name='delete'),
    path('deleteActivity/<int:delete_id>/', views.deleteActivity, name='deleteActivity'),

]
