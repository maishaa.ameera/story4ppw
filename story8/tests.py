from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from .views import searchbook, fungsidata
from django.apps import apps
from .apps import Story8Config

# Create your tests here.

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')

class Story8UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.response = self.client.get('/story8/')
        self.page_content = self.response.content.decode('utf8')

    def test_story8_url_exists(self):
        self.assertEqual(self.response.status_code, 200)

    def test_story8_check_template_used(self):
        self.assertTemplateUsed(self.response, 'searchbook.html')
    
    def test_story8_check_function_used(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, searchbook)
    
  

    


