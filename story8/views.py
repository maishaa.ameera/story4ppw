from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
import json
from django.core import serializers
import requests
 
# Create your views here.
def searchbook(request):
    return render( request, 'searchbook.html')

def fungsidata(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    print(ret.content)
    return JsonResponse(ret.json())