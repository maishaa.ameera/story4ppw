from django.urls import path
from . import views
 
app_name = 'story8'
 
urlpatterns = [
    path('',views.searchbook, name='searchbook'),
    path('data/', views.fungsidata, name='fungsidata'),
]
