from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import logIn, signUp, logOut
from django.http import HttpRequest
from .apps import Story9Config
from .forms import LoginForm, SignUpForm
from django.apps import apps

# Create your tests here.
class Story9(TestCase) :
    def setUp(self) :
        self.client = Client()
        self.login = reverse("story9:login")
        self.signup = reverse("story9:signup")
        self.logout = reverse("story9:logout")

    def test_status_code_and_template_login(self): 
        response = self.client.get(self.login)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_status_code_and_template_signup(self): 
        response = self.client.get(self.signup)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')

    def test_status_code_and_logout(self): 
        response = self.client.get(self.logout)
        self.assertEqual(response.status_code, 302)


    def test_function_used_by_login(self) :
        found = resolve(self.login)
        self.assertEqual(found.func, logIn)

    def test_function_used_by_signup(self) :
        found = resolve(self.signup)
        self.assertEqual(found.func, signUp)

    def test_function_used_by_logout(self) :
        found = resolve(self.logout)
        self.assertEqual(found.func, logOut)    
    
    def test_check_login_page_have_form(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)

def test_login_test(self):
        response = self.client.post('/account/login/', data={'username' : 'aku123', 'password' : 'halohalo'})
        self.assertEqual(response.status_code,404)

def test_register_already_exist_test(self):
        response = self.client.post('/story9/signup/', data={'first_name': "aku",
            'last_name': "123",
            'username': "aku123",
            'password1': "halohalo",
            'password2': "halohalo"})
        self.assertEqual(response.status_code,404)