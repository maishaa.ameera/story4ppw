from django.test import TestCase
from django.test import Client
from django.apps import apps
from .apps import Story7Config
from .views import accordion
from django.urls import resolve, reverse



# Create your tests here.
class Story7(TestCase) :
    def setUp(self):
        self.client = Client()
        self.accordion = reverse("story7:accordion")

    def test_status_code_and_accordion(self): 
        response = self.client.get(self.accordion)
        self.assertEqual(response.status_code, 200)

    def test_template_used_accordion(self): 
        response = self.client.get(self.accordion)
        self.assertTemplateUsed(response, 'accordion.html')

    def test_function_used_by_accordion(self) :
        found = resolve(self.accordion)
        self.assertEqual(found.func, accordion)

    def test_apps_is_true(self) :
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')


