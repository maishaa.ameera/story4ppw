from django import forms


SEMESTER = [('Gasal','Gasal'),('Genap','Genap')]

class ScheduleForm(forms.Form):
    coursename = forms.CharField(
        label = "Course Name", 
        widget = forms.TextInput(
            attrs={
            'placeholder' : 'Enter course name',
            'type' : 'text',
            'id' : 'coursename',
            'class':'form-control',
            'required': True,

            }
        )
    )

    lecturer = forms.CharField(
        label = "Lecturer Name", 
        widget=forms.TextInput(
            attrs={
            'placeholder' : 'Enter lecturer name',
            'type' : 'text',
            'id' : 'lecturer',
            'class': 'form-control',
            'required': True,
    }))

    credit = forms.CharField(
        label = "Credit (SKS)", 
        widget=forms.TextInput(
         attrs={
            'placeholder' : 'Enter number of credits',
            'type' : 'text',
            'id' : 'credit',
            'class': 'form-control',
            'required': True,
    }))

    description = forms.CharField(
        label = "Course description",
         widget=forms.TextInput(
            attrs={
        'class': 'form-control',
        'type' : 'text',
        'id' : 'description',
        'placeholder': 'Enter course description',
        'required': True,
    }))

    semester = forms.CharField(
        label = "Semester", 
        widget=forms.Select(choices=SEMESTER)
        
    
    )
    year = forms.CharField(
        label = "Year",
        widget=forms.TextInput(
            attrs={
        'class': 'form-control',
        'type' : 'text',
        'id' : 'year',
        'placeholder': 'ex: 2019/2020',
        'required': True,
    }))
    room = forms.CharField(
        label = "Classroom Number",
        widget=forms.TextInput(
            attrs={
        'class': 'form-control',
        'type' : 'text',
        'id' : 'room',
        'placeholder': 'Enter classroom number',
        'required': True,
    }))
