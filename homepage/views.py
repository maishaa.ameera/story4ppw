from django.shortcuts import render ,redirect
from .models import Schedule
from . import forms
from .forms import ScheduleForm

# Create your views here.
def index(request):
    return render(request, 'index.html')

def hobby(request):
    return render(request, 'hobby.html')

def gallery(request):
    return render(request, 'gallery.html')

def homeschedule(request):
    return render(request, 'homeschedule.html')

def form(request):
    form_matkul = forms.ScheduleForm()
    if request.method == 'POST':
        form_matkul_input = forms.ScheduleForm(request.POST)
        if form_matkul_input.is_valid():
            data = form_matkul_input.cleaned_data
            matkul_input = Schedule()
            matkul_input.coursename = data['coursename']
            matkul_input.lecturer = data['lecturer']
            matkul_input.credit = data['credit']
            matkul_input.description = data['description']
            matkul_input.semester = data['semester']
            matkul_input.year = data['year']
            matkul_input.room = data['room']
            matkul_input.save()
            current_data = Schedule.objects.all()
            return redirect('homepage:homeschedule')  
        else:
            current_data = Schedule.objects.all()
            return render(request, 'form.html',{'form':form_matkul, 'status':'failed','data':current_data})
    else:
        current_data = Schedule.objects.all()
        return render(request, 'form.html',{'form':form_matkul,'data':current_data})

def responseform(request):
    data = Schedule.objects.all()
    return render(request,'responseform.html',{'responseform':data})

def delete(request, delete_id):
    try:
        jadwal_to_delete = Schedule.objects.get(pk = delete_id)
        jadwal_to_delete.delete()
        return redirect('homepage:responseform')
    except:
        return redirect('homepage:responseform')





