from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('hobby/', views.hobby, name='hobby'),
    path('gallery/', views.gallery, name='gallery'),
    path('home-schedule/', views.homeschedule, name='homeschedule'),
    path('form/', views.form, name='form'),
    path('responseform/', views.responseform, name='responseform'),
    path('delete/P<int:delete_id>/',views.delete, name='delete'),
    
    # dilanjutkan ...
]
