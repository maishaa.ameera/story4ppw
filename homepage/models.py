from django.db import models

# Create your models here.
SEMESTER = [('Gasal','Gasal'),('Genap','Genap')]


class Schedule(models.Model):
    coursename = models.CharField(max_length = 100)
    lecturer = models.CharField(max_length = 100)
    credit = models.CharField(max_length = 100)
    description = models.CharField(max_length = 1000, null = True)
    semester = models.CharField(max_length = 1000, choices=SEMESTER, default = 'Choose..' )
    year = models.CharField(max_length = 100)
    room = models.CharField(max_length = 100)


    def __str__(self):
        return self.coursename
